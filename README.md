# Ansible scripts to provision and deploy Odoo

These are [Ansible](http://docs.ansible.com/ansible/) playbooks (scripts) for managing an [Odoo](https://github.com/odoo/odoo) server.

## Requirements

You will need Ansible on your machine to run the playbooks.
These playbooks will install the PostgreSQL database, NodeJS and Python virtualenv to manage python packages.

It has currently been tested on **Ubuntu 16.04 Xenial (64 bit)**.


Install dependencies running:
```
ansible-galaxy install -r requirements.yml
```

### Inventory

You need define a inventory folder with the `hosts` file and the `group_vars` and `host_vars` file. See the Ansible doc about inventories: https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html
You can see an example in: https://gitlab.com/femprocomuns/odoo-femprocomuns-provisioning

After create this folders extructure, you can execute this playbooks with the `-i <YOUR_INVENTORY_FILE>` argument pointing to your `hosts` file.

Please look at example for more information.

## Playbooks

### sys_admins.yml

This playbook uses the community role [sys-admins-role](https://github.com/coopdevs/sys-admins-role).

This playbook will prepare the host to allow access to all the system administrators.

```yaml
# playbooks/my_playbook.yml
- name: Create all the users for system administration
  roles:
    - role: coopdevs.sys-admins-role
      vars:
        sys_admin_group: sysadmin
        sys_admins: "{{ system_administrators }}"
```

In each environment (`dev`, `staging`, `production`) we can find the list of users that will be created as system administrators.

We use `host_vars` to declare per environment variables:
```yaml
# <YOUR_INVENTORY_FOLDER>/inventory/host_vars/<YOUR_HOST>/config.yml

system_administrators:
  - name: pepe
    ssh_key: "../pub_keys/pepe.pub"
    state: present
    - name: paco
    ssh_key: "../pub_keys/paco.pub"
    state: present
```

Before execute the playbook, be sure that the needed keys are present in `pub_keys` folder.
The first time you run it against a brand new host you need to run it as `root` user.
You'll also need passwordless SSH access to the `root` user.
```
ansible-playbook playbooks/sys_admins.yml --limit=<environment_name> -u root -i <YOUR_INVENTORY_FILE>
```

For the following executions, the script will assume that your user is included in the system administrators list for the given host.

For example in the case of `development` environment the script will assume that the user that is running it is included in the system administrators [list](https://github.com/coopdevs/timeoverflow-provisioning/blob/master/inventory/host_vars/local.timeoverflow.org/config.yml#L5) for that environment.

To run the playbook as a system administrator just use the following command:
```
ansible-playbook playbooks/sys_admins.yml --limit=dev
```
Ansible will try to connect to the host using the system user. If your user as a system administrator is different than your local system user please run this playbook with the correct user using the `-u` flag.
```
ansible-playbook playbooks/sys_admins.yml --limit=dev -u <username>
```

### Provision
`provision.yml` - Installs and configures all required software on the server.

This is the main playbook. This playbook install all the Odoo dependencies and the Odoo version that you define in the inventory. Install and update also the modules defined in the inventory repository.

The playbook use different community roles to perform the complete Odoo installation. Also use our own roles to manage the user and database creation.

#### Community roles:
* Security - [geerlingguy.security](https://github.com/geerlingguy/ansible-role-security)

This role change the minimum security configuration. More information in the role description.

* PostgreSQL - [geerlingguy.postgresql](https://github.com/geerlingguy/ansible-role-postgresql)

Role to install PostgreSQL in the different environments.

* Certbot NGINX - [coopdevs.certbot_nginx](https://github.com/coopdevs/certbot_nginx)

This role install and configure the NGINX plugin of Certbot to allow renew the certificate when NIGNX is running.
Alto create the certificate if it not exists
More info in the role description.

You need declare the next variables:
```yaml
certificate_authority_email:        # Email to associate the certificate
```

* NGINX - [jdauphant.nginx](https://github.com/jdauphant/ansible-role-nginx)

Install NGINX package in different environments.

* Odoo - [coopdevs.odoo-role](https://github.com/coopdevs/odoo-role)

This is the role that install and configure the Odoo server.
You need declare the next vars:
```yaml
odoo_db_admin_password:             # DB Master password. We recomend encrypt this var in a secrets file
odoo_core_modules:                  # List of modules to install or update from Odoo core.
odoo_community_modules              # List of modules to install or update from Odoo community.
odoo_db_name:                       # DB name
odoo_edition:                       # Odoo edition (We recommend OCA)
odoo_role_odoo_db_name: "{{ odoo_db_name }}"
odoo_role_odoo_edition: "{{ odoo_edition }}"
```

> To install the modules, first you need to be sure that the modules are in the scope of Odoo. You can deploy the modules in the server with `pip`. To the community roles, you need define a `requirements.txt` inside the *inventory* folder.

#### Roles
* PostgreSQL Odoo - `postgresql-odoo`

This role is a workaround to create the database and the user with the needed permissions.
You need define the next vars:
```yaml
odoo_db_name:                       # DB name
odoo_db_user:                       # DB user
```

> To avoid install NGINX and the NGINX Certbot plugin, we can define a var as:
```yaml
development_environment: true
```

* Backups - `backups`

This role overwrites the strategy to obtain data to be backed up, so that we use Odoo's own way.
You need to define these variables, consider doing it in an Ansible vault. Example values.
```yaml
# Password for postgresql unprivileged backups user
backups_role_postgresql_user_password: "seraph babylon humanely boccie compeer revers"

# Remote bucket URL in restic format
# Example for backblaze:  "b2:bucketname:path/to/repo"
# Example for local repo: "/var/backups/repo"
backups_role_restic_repo_url: "b2:unique-bucket-name:path/to/restic/repo/inside/the/bucket"
backups_role_restic_repo_password: "rwqlenwhat7a7rand0m7passw0rd7ss2"

# Backblaze "application" or bucket credentials
backups_role_b2_app_key_id: "00f00ba12bebo000000000023"
backups_role_b2_app_key: "uyoWyGV8ueNUW0ea98fkPAN1CxMKijK"
```

Backups are enabled by default, but can be disabled by setting `backups_role_enabled: false`. You may want to do it at your testing instance, or for dev environments. As an alternative to host vars files, you can also use the ansible command-line paramenter `--extra-vars`, or `-e` for short.

```sh
# Append this option to your ansible-playbook call
#+ in order to disable backups for this play.
-e '{backups_role_enabled: false}'
```

Note: there is a simpler syntax in the form `-e "key=value"`, but it only supports strings and the role condition expects a boolean. This means that it will not tell the difference between non-empty strings, e.g. `"true"` and `"false"` both evaluate to `true`. This is why we use the JSON syntax, that allows booleans. It's ok for this JSON parser to avoid quotes on the keys.


# Installation instructions

### Step 1 - sys_admins

The **first time** thet execute this playbook use the user `root`

`ansible-playbook playbooks/sysadmins.yml --limit <environment_name> -u root`

All the next times use your personal system administrator user:

`ansible-playbook playbooks/sysadmins.yml --limit <environment_name> -u USER`

USER --> Your sysadmin user name.

### Step 2 - Provision

`ansible-playbook playbooks/provision.yml -u USER`

USER --> Your sysadmin user name.

# Users Management

### Default User `odoo`

It is the owner of the `odoo` group and can execute the next commands without password in `sudo` mode:

```
sudo systemctl start odoo.service
sudo systemctl stop odoo.service
sudo systemctl status odoo.service
sudo systemctl restart odoo.service
```

**It can execute the Odoo service and restart if is needed. Is the user to manage the Odoo process and to install and update new modules.**

### System Administrators

The sysadmins are the superusers of the environment.
They have `sudo` access without password for all commands.

**They can execute `sys_admins.yml`, `provision.yml`, `deploy.yml` and `deploy_custom_modules.yml` playbooks.**

## DB Admin Password

Add password to the `super admin` that manage the dbs.
In `inventory/host_vars/host_name/secrets.yml` add the key `admin_passwd` to protect the creation and management of dbs.
`secrets.yml` is a encrypted vault. Run `ansible-vault edit secrets.yml` to change this password.

# Development

In the development environment (`local.odoo.net`) you must use the sysadmin user `odoo`.

`ssh odoo@local.odoo.net`

## Using LXC Containers

In order to create a development environment, we use the [devenv](https://github.com/coopdevs/devenv) tool. It uses [LXC](https://linuxcontainers.org/) to isolate the development environment.

This tool search a configuration dotfile in the project path. You must create a `.devenv`  file with the next declared variables:

```yaml
# odoo-provisioning/.devenv
NAME="odoo"
DISTRIBUTION="ubuntu"
RELEASE="bionic"
ARCH="amd64"
HOST="local.$NAME.coop"
PROJECT_NAME="odoo"
PROJECT_PATH="${PWD%/*}/$PROJECT_NAME"
DEVENV_USER="odoo"
DEVENV_GROUP="odoo"
```

You can modify them to change the environment.

Run the following command: `devenv`

Devenv will:

* Create container
* Mount your project directory into container in `/opt/<project_name>`
* Add container IP to `/etc/hosts`
* Create `odoo` group with same `gid` of project directory
* Create `odoo` user with same `uid` and `gid` of project directory
* Add system user's SSH public key to `odoo` user
* Install python2.7 in container
* Run `sys_admins.yml` playbook

When the execution ends, you have a container ready to execute sys_admins playbook, provision and deploy the app.
